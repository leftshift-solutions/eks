# EKS Sandbox

## Requirements
- terraform
- aws-iam-authenticator
- AWS credentials setup on your local machine e.g.: `~/.aws/credentials`, adjust accordingly in the `providers.tf`

## Usage
- `terraform init` Setup terraform 
- `terraform plan` check if configs are ok
- `terraform apply` Start Cluster
	- apply configmap
	- setup kube config
- `terraform destroy` Delete Cluster